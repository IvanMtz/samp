$(document).on("ready", function(){
    registerMessages();
    $.ajax({"cache":false});
    setInterval("loadMessages()", 500);
    
    $("#inicio").on("click", function(e){
        e.preventDefault();
        document.getElementById("maintitulo").innerHTML="<p style='margin-top: 5px;'>Pagina principal</p>";
        document.getElementById("maintexto").innerHTML="<p style='margin-top: 5px;'>Contenido principal</p>";
    })
    $("#perfil").on("click", function(e){
        e.preventDefault();
        formularioPerfil();
        datosPerfil();
    })
    $("#btnregistrarme").on("click", function(e){
        e.preventDefault();
        formularioRegistro();
    })
    $("#btningresar").on("click", function(e){
        e.preventDefault();
        formularioLogin();
    })
});

var registerMessages = function(){
    $("#enviar").on("click", function(e){
        e.preventDefault();
        var frm = $("#formChat").serialize();
        if(document.getElementById("mensaje").value != ""){
             $.ajax({
                type: "POST",
                url: "php/register.php",
                data: frm
            }).done(function(info){
                document.getElementById("mensaje").value = "";
                document.getElementById("auxiliar").innerHTML="<embed loop='false' src='misc/beep.mp3' hidden='true' autoplay='true' volume='0.5'>";
                var frag = document.getElementById("auxiliar").innerHTML="<embed loop='false' src='misc/beep.mp3' hidden='true' autoplay='true' volume='0.5'>";
            })           
        }

    })
}

var loadMessages = function(){
    $.ajax({
        type: "POST",
        url: "php/conversacion.php"
    }).done(function(info){
        $("#mensajes").html(info);
    });
}

var datosPerfil = function(){
    $.ajax({
        type: "POST",
        url: "php/datosPerfil.php"
    }).done(function(info){
        $("#auxiliar").html(info);
    });
}    

var formularioRegistro = function(){
    document.getElementById("maintitulo").innerHTML="<p style='margin-top: 5px;'>Formulario de registro</p>";
    document.getElementById("maintexto").innerHTML="<div style='display:inline-block;'><form id='formReg' method='post'><input type='text'  style='width: 300px;padding: 10px;margin:5px;border-radius:5px;' name='nombre' id='nombre' placeholder='Nombre de Usuario'><br><input type='password' style='width: 300px;padding: 10px;margin:5px;border-radius:5px;'  name='clave' id='clave' placeholder='Contraseña'><br><input type='email'  style='width: 300px;padding: 10px;margin:5px;border-radius:5px;' name='email' id='email' placeholder='Correo'><br><input class='btnreg' type='submit' name='btnRegistrar' id='btnRegistrar' style='width: 300px;border:none;padding:10px;margin:5px;border-radius:5px;' value='Registrarme'></form></div><div id='notif' name='notif' style='text-align:center;'></div>";
    callReg();
}

var callReg = function(){
    $("#btnRegistrar").on("click", function(e){
        e.preventDefault();
        var frm = $("#formReg").serialize();
        $.ajax({
            type: "POST",
            url: "php/registrar.php",
            data: frm
        }).done(function(info){ 
            $("#notif").html(info);
        })
    })    
}

var callLogin = function(){
    $("#btnlogin").on("click", function(e){
        e.preventDefault();
        var frm = $("#formLogin").serialize();
        $.ajax({
            type: "POST",
            url: "php/login.php",
            data: frm
        }).done(function(info){ 
            $("#notif").html(info);
        })
    })    
}

var callPerfil = function(){
    $("#btnEditar").on("click", function(e){
        e.preventDefault();
        var frm = $("#formPerfil").serialize();
        $.ajax({
            type: "POST",
            url: "php/perfil.php",
            data: frm
        }).done(function(info){ 
            $("#notif").html(info + "<meta http-equiv='Refresh' content='1;url=index.php'>");
        })
    })    
}

var color = function(){
    $.ajax({
        type: "POST",
        url: "php/color.php"
    }).done(function(info){
        $("#notif").html(info);
    });      
}

var formularioLogin = function(){
    document.getElementById("maintitulo").innerHTML="<p style='margin-top: 5px;'><br>Iniciar sesion</p>";
    document.getElementById("maintexto").innerHTML="<div style='display:inline-block;'><form id='formLogin' method='post' onsubmit='return validarlogin();'><input type='text' style='width: 300px;padding:10px;margin:5px;border-radius:5px;' name='nombre' id='nombre' placeholder='Usuario'><br><input type='password' style='width: 300px;padding:10px;margin:5px;border-radius:5px;' name='clave' id='clave' placeholder='Contraseña'><br><input type='submit' class='btnreg' style='width: 300px;padding:10px;margin:5px;border-radius:5px;border: none;' id='btnlogin' name='btnlogin' value='Ingresar'></form></div><div id='notif' name='notif' style='text-align:center;'></div>";
    callLogin();
}

var formularioPerfil = function(){
    document.getElementById("maintitulo").innerHTML="<p style='margin-top: 5px;'><br>Configuracion de usuario</p>";
    document.getElementById("maintexto").innerHTML="<div style='display:inline-block;'><form id='formPerfil' method='post' onsubmit='return validarPerfil();'><label for='nombre'> User: </label><input type='text' style='width: 300px;padding:10px;margin:5px;border-radius:5px;' name='nombre' id='nombre'><br><label for='email'>Email: </label><input type='text' style='width: 300px;padding:10px;margin:5px;border-radius:5px;' name='email' id='email'><br><label for='clave'>Clave: </label><input type='password' style='width: 300px;padding:10px;margin:5px;border-radius:5px;' name='clave' id='clave'><br><input type='color' style='width: 50px;padding:10px;margin:5px;border-radius:5px;' name='color' id='color' placeholder='Color'><br><input type='submit' class='btnreg' style='width: 300px;padding:10px;margin:5px;border-radius:5px;border: none;' id='btnEditar' name='btnEditar' value='Editar'></form></div><div id='notif' name='notif' style='text-align:center;'></div>";
    callPerfil();
}
function validar(){
    var nombre, clave, email;
    nombre = document.getElementById("nombre").value;
    clave = document.getElementById("clave").value;
    email = document.getElementById("email").value;
    
    if(nombre === "" || clave === "" || email === ""){
        alert("Debe llenar todos los campos");
        formularioRegistro();
        return false;
    }else if(nombre.length>45){
        alert("El nombre no puede tener mas de 45 caracteres");
        formularioRegistro();
        return false;
    }else if(clave.length>45){
        alert("La clave no puede tener mas de 45 caracteres");
        formularioRegistro();
        return false;
    }else if(email.length>45){
        alert("El correo no puede tener mas de 45 caracteres");
        formularioRegistro();
        return false;
    }
}

$("#logout").on("click", function(e){
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "php/logout.php"
        }).done(function(info){
            $("#maintexto").html(info);
        })   
})