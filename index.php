<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title>LAGaming</title>
        <link rel="stylesheet" href="css/estilos.css">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="css/jquery.mCustomScrollbar.css">
    </head>
    <body>
    
       <div class="contenido">
           <div class="header">
              <div class="logo">
                  <img src="img/logo.png" alt="">
              </div>
              <div class="menu">
                   <ul>
                       <li id="inicio">Inicio</li>
                       <li id="foro">Foro</li>
                       <li id="contacto">Contacto</li>
                       <?php
                       session_start();
                       if(isset($_SESSION['s_usuario'])){
                           echo "<li id='perfil'>Mi Perfil</li>";
                       }
                       ?>
                    </ul>
              </div>
              <div class="perfil">
                 <div class="info">
                    <?php
                        if(isset($_SESSION['s_usuario'])){
                            echo "<p style='font-size: 12px;'> ".$_SESSION['s_usuario']."</p>";
                            echo "<p id='logout' name='logout' style='font-size: 12px';>Salir</p>";
                        }else{
                            echo "<p id='btnregistrarme'>Registrarme</p>";
                            echo "
                     <p id='btningresar'>Ingresar</p>";
                        }
                     ?>
                 </div>
                 <div class="foto">
                    <!--<img src="img/user.png" alt="">-->
                 </div>
              </div>
           </div>
           <div class="medio">
               <div class="chat">
                   <div class="titulo">
                       <br><b>Chat</b>
                   </div>
                   <div id="mensajes" class="mensajes">
                       <p style="font-size:8px;">Cargando mensajes...</p>
                   </div>
                   <div class="texto">
                       <?php
                       if(isset($_SESSION['s_usuario'])){
                           echo "<form id='formChat' action='POST'>";
                           echo "<input type='text' class='msgbox' name='mensaje' id='mensaje' placeholder='Mensaje...'>";
                           echo "<input class='btnenviar' name='enviar' id='enviar' type='submit' value='Enviar'></form>";
                       }else{
                           echo "<p>Debes iniciar sesion para poder usar el chat</p>";
                       }
                    
                       ?>
                      <!--<form id="formChat" action="POST">
                        <input type="text" class="msgbox" name="mensaje" id="mensaje" placeholder="Mensaje...">
                        <input class="btnenviar" name="enviar" id="enviar" type="submit" value="Enviar">
                      </form>-->
                   </div>
               </div>
               <div class="main">
                   <div id="maintitulo" class="titulo"><br>
                   Latino America Gaming
                   </div>
                   <div id="maintexto" class="texto">
                       <div class="cmaintexto">
                           <img src="https://i.ytimg.com/vi/q6h6T7yvqOs/maxresdefault.jpg"  width="600px" height="300px"; alt="">
                       </div>
                   </div>
                   <div id="auxiliar">
                       
                   </div>
               </div>
           </div>
       </div>
    
    <script src="js/jquery-latest.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/jquery-2.1.0.min.js"></script>
    <script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script>
        (function($){
            $(window).on("load", function(){
                $(".mensajes").mCustomScrollbar({
                    theme:"rounded",
                    scrollButtons:{enable:true}
                })
            })
        })
    </script>
    </body>
</html>